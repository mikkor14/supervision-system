﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupervisionSystem.Model
{
    class Student
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public Supervisor Supervisor { get; set; }

        public int SupervisorId { get; set; }
    }
}
