﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SupervisionSystem.Model
{
    class slNameDBContext : DbContext
    {
        public DbSet<Supervisor> Supervisors { get; set; }
        public DbSet<Student> Students { get; set; }
    }
}
