﻿namespace SupervisionSystem
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DBGridView = new System.Windows.Forms.DataGridView();
            this.nameInput = new System.Windows.Forms.TextBox();
            this.nameInputLabel = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.SuperNameListBox = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.toJSONButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DBGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // DBGridView
            // 
            this.DBGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DBGridView.Location = new System.Drawing.Point(269, 12);
            this.DBGridView.MultiSelect = false;
            this.DBGridView.Name = "DBGridView";
            this.DBGridView.RowHeadersWidth = 62;
            this.DBGridView.RowTemplate.Height = 28;
            this.DBGridView.Size = new System.Drawing.Size(505, 236);
            this.DBGridView.TabIndex = 0;
           // this.DBGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DBGridView_CellContentClick);
            // 
            // nameInput
            // 
            this.nameInput.Location = new System.Drawing.Point(82, 36);
            this.nameInput.Name = "nameInput";
            this.nameInput.Size = new System.Drawing.Size(140, 26);
            this.nameInput.TabIndex = 1;
            //this.nameInput.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // nameInputLabel
            // 
            this.nameInputLabel.AutoSize = true;
            this.nameInputLabel.Location = new System.Drawing.Point(12, 36);
            this.nameInputLabel.Name = "nameInputLabel";
            this.nameInputLabel.Size = new System.Drawing.Size(51, 20);
            this.nameInputLabel.TabIndex = 3;
            this.nameInputLabel.Text = "Name";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(16, 77);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(220, 31);
            this.addButton.TabIndex = 5;
            this.addButton.Text = "Add student";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(607, 278);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(150, 31);
            this.deleteButton.TabIndex = 8;
            this.deleteButton.Text = "Delete Selected";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // SuperNameListBox
            // 
            this.SuperNameListBox.FormattingEnabled = true;
            this.SuperNameListBox.ItemHeight = 20;
            this.SuperNameListBox.Location = new System.Drawing.Point(12, 278);
            this.SuperNameListBox.Name = "SuperNameListBox";
            this.SuperNameListBox.Size = new System.Drawing.Size(308, 84);
            this.SuperNameListBox.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(85, 255);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Existing Supervisors";
           // this.label3.Click += new System.EventHandler(this.Label3_Click);
            // 
            // toJSONButton
            // 
            this.toJSONButton.Location = new System.Drawing.Point(12, 377);
            this.toJSONButton.Name = "toJSONButton";
            this.toJSONButton.Size = new System.Drawing.Size(100, 31);
            this.toJSONButton.TabIndex = 12;
            this.toJSONButton.Text = "To JSON";
            this.toJSONButton.UseVisualStyleBackColor = true;
            this.toJSONButton.Click += new System.EventHandler(this.ToJSONButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.toJSONButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.SuperNameListBox);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.nameInputLabel);
            this.Controls.Add(this.nameInput);
            this.Controls.Add(this.DBGridView);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.DBGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DBGridView;
        private System.Windows.Forms.TextBox nameInput;
        private System.Windows.Forms.Label nameInputLabel;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.ListBox SuperNameListBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button toJSONButton;
    }
}

