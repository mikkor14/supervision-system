﻿using SupervisionSystem.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Data.Entity;

namespace SupervisionSystem
{
    public partial class Form1 : Form
    {

        slNameDBContext nameDBContext = new slNameDBContext();


        public Form1()
        {
            InitializeComponent();


            BindingSource bindingSupervisor = new BindingSource();
            SuperNameListBox.DataSource = bindingSupervisor;


            var resultSetSupervisors = from s in nameDBContext.Supervisors
                                       select new { s.Id, Name = s.Name };


            bindingSupervisor.DataSource = resultSetSupervisors.ToList();

            refreshGridData();

            SuperNameListBox.Refresh();
            DBGridView.Refresh();

        }

        public void refreshGridData()
        {
            BindingSource bindingStudent = new BindingSource();
            DBGridView.DataSource = bindingStudent;


            var resultSetStudents = from s in nameDBContext.Students
                                    select new { s.Id, Name = s.Name };

            bindingStudent.DataSource = resultSetStudents.ToList();

            DBGridView.Refresh();

        }

        private void AddButton_Click(object sender, EventArgs e)
        {

            if (nameInput.Text != null)
            {


                //This chunk of code makes a list of all Supervisor ID's to use as FK for new Student because the DB does not sync the ID's
                var resultSetSupervisors = from s in nameDBContext.Supervisors
                                           select new { s.Id };

                var supervisorList = resultSetSupervisors.ToList();

                String[] supervisorStringArray = new String[SuperNameListBox.Items.Count];

                List<int> supervisorIdList = new List<int>();

                for (int i = 0; i < supervisorList.Count; i++)
                {
                    supervisorStringArray[i] = supervisorList[i].ToString();
                    String substr1 = supervisorStringArray[i].Replace("{ Id = ", "");
                    String substr2 = substr1.Replace("}", "");
                    int id = Int32.Parse(substr2);
                    supervisorIdList.Add(id);
                }



                Student stud = new Model.Student { Name = nameInput.Text, SupervisorId = supervisorIdList.ElementAt(0) };
                supervisorIdList.RemoveAt(0);
                nameDBContext.Students.Add(stud);
                nameDBContext.SaveChanges();

                refreshGridData();

            }

        }


        private void ToJSONButton_Click(object sender, EventArgs e)
        {
            String JSONString = JsonConvert.SerializeObject(nameDBContext.Supervisors, Formatting.Indented);
            MessageBox.Show(JSONString);
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            String selectedStud = DBGridView.SelectedCells[0].Value.ToString();

            //Check if user seleted an ID or Name
            if (Regex.IsMatch(selectedStud, @"^\d+$"))
            {
                Student stud = nameDBContext.Students.Find(Int32.Parse(selectedStud));

                nameDBContext.Students.Remove(stud);
                nameDBContext.SaveChanges();

            } else
            {
                MessageBox.Show("Please select the ID of the student you want to delete");
            }

            refreshGridData();

        }
    }
}
