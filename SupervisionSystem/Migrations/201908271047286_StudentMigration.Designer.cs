// <auto-generated />
namespace SupervisionSystem.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class StudentMigration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(StudentMigration));
        
        string IMigrationMetadata.Id
        {
            get { return "201908271047286_StudentMigration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
