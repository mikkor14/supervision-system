namespace SupervisionSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateNames : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Students", new[] { "supervisorId" });
            CreateIndex("dbo.Students", "SupervisorId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Students", new[] { "SupervisorId" });
            CreateIndex("dbo.Students", "supervisorId");
        }
    }
}
