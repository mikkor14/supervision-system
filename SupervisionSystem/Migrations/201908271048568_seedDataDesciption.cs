namespace SupervisionSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedDataDesciption : DbMigration
    {
        public override void Up()
        {

            Sql("INSERT INTO Supervisors(Name) VALUES('Johnny')");
            Sql("INSERT INTO Supervisors(Name) VALUES('Danny')");
            Sql("INSERT INTO Supervisors(Name) VALUES('Peter')");

        }
        
        public override void Down()
        {

            Sql("DELETE FROM Supervisors WHERE Name ='Johnny'");
            Sql("DELETE FROM Supervisors WHERE Name ='Danny'");
            Sql("DELETE FROM Supervisors WHERE Name ='Peter'");
        }
    }
}
